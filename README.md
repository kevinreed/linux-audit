# Linux audit configuration

`auditd` configuration that makes resonable tradeoff between visibility and
performance. Please, note, some of the rules rely on SELinux subject tags for
classification. SELinux does not have to be in enforcement mode, permissive
is good enough, but it should not be disabled.

## Configuration

Place the `audit.rules` file into `/etc/audit` and make sure, audit susbsystem
is enabled.

## Links

- [Tuning auditd: high-performance Linux Auditing](https://linux-audit.com/tuning-auditd-high-performance-linux-auditing/)
- [UK DS audit.rules](https://github.com/gds-operations/puppet-auditd/pull/1#issuecomment-30327163)
- [Generic ruleset from linux audit project](https://github.com/linux-audit/audit-userspace/tree/master/rules)
- [Audit Record Types for RHEL
  7](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-audit_record_types)
- [RHEL Audit System Reference](https://access.redhat.com/articles/4409591)
